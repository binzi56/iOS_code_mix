# iOS_code_mix
马甲包混淆工程

# 原工程地址
[https://github.com/klaus01/KLGenerateSpamCode](https://github.com/klaus01/KLGenerateSpamCode)

# 新增功能
除了已有的图片资源递增修改、修改工程名、类前缀修改(修改了遍历方案)外，还加了一些骚东西

0、直接在工程中添加垃圾代码，垃圾代码的规则可自己修改脚本代码自定义

1、混淆随机添加垃圾代码、参数

2、修改方法名前缀

3、修改方法名，使用plist文件创建原始方法名仓库，共有6^6个方法名可以配置，随机方法名配参数

4、删除垃圾代码。以脚本前缀为索引

5、混淆概率

# 注意点

1、这个工程里的方法名前缀只适应我们自己的工程，因为我们的工程方法名是前缀_xxx这类格式的，所以不要再问为什么没有资源改变了兄dei。没有执行的代码，麻烦看看main函数里是不是注释掉了，先看代码，再发问。

2、api名字是随机从6^6个方法名生成的，可以在plist文件中修改

# 使用方法

和原来的轮子一样，先配置启动参数再运行，如图

![image_0](http://ok9lu0v73.bkt.clouddn.com/6C05CE31-D951-4D90-81D3-0314EB267C9E.png)
参数解释：

1.工程代码的绝对路径

2.-modifyProjectName [原工程名]>[新工程名]

3.-modifyClassNamePrefix [xcodeproj文件的绝对路径，不是pod安装后的那个打开文件] [旧类前缀]>[新类前缀]

4.-spamCodeOut

5.-ignoreDirNames [需要忽略的文件夹],[需要忽略的文件夹]             注意，Pods文件夹不在混淆范围内，不需要写

6.-handleXcassets              (混淆图片文件)

7.-deleteComments             (删除多余的空格和注释)

8.-chageAPIPrefix [旧方法名前缀]>[新方法名前缀]              注意，前缀要有“_”才能被识别，如果之前工程中没有xx_下划线开头来命名方法的，此项不要勾选

9.-modifyAPIName         改变api名字，注意是随机的，这个更改最好不要提交，只用来上架，一次性操作，否则可能增加后续维护的负担（看不懂方法名了）

***此工程可以选择混淆概率，修改工程中kPercent数值。***

# 详情
有关此工程的设计详情请看这篇文章[传送门](http://www.imyuyang.com/2018/02/15/iOS%E9%A9%AC%E7%94%B2%E5%8C%85%E6%B7%B7%E6%B7%86%E6%96%B9%E6%A1%88/)

# 实际测试

![image_1](http://ok9lu0v73.bkt.clouddn.com/i18%5Epimgpsh_fullsize_distr.png)


--------
#### *** 分割线下方阅读 ***
--------

# 1.参考
[“苹果2.1大礼包”审核被拒，这有份iOS马甲包混淆方案](http://www.niaogebiji.com/article-17212-1.html)

# 2.推荐阅读
[IOS马甲包混淆](https://blog.csdn.net/lyzz0612/article/details/80390362)

# 3.代码运行截图
![iOS代码混淆](https://upload-images.jianshu.io/upload_images/1893416-c5f856536e6e601f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
